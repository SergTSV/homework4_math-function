const app = document.getElementById('app')
const inputFirstNumber = app.querySelector('#firstNumber');
const inputSecondNumber = app.querySelector('#secondNumber');
const selectOperation = app.querySelector('#operation');
const btnCalc = app.querySelector('#calc');

const calc = () => {
    btnCalc.addEventListener('click', () => {
             if (!!inputFirstNumber.value && !!inputSecondNumber.value && !isNaN(inputFirstNumber.value) && !isNaN(inputSecondNumber.value)) {
            const operations = {
                '+': +inputFirstNumber.value + +inputSecondNumber.value,
                '-': +inputFirstNumber.value - +inputSecondNumber.value,
                '*': +inputFirstNumber.value * +inputSecondNumber.value,
                '/': +inputFirstNumber.value / +inputSecondNumber.value,
            };
            console.log(operations[selectOperation.value]);
        } else {
            alert('Input Error. Enter the data again');
        }
    });
};

calc();
